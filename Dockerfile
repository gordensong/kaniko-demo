FROM ccr.ccs.tencentyun.com/w7team/swoole:fpm-php8.0

ENV WEB_PATH /home/www/

WORKDIR $WEB_PATH

COPY nginx.conf /usr/local/nginx/conf/vhost/

COPY --chown=1000:1000 . $WEB_PATH

RUN chmod -R 755 $WEB_PATH

CMD ["/bin/sh", "start.sh"]
